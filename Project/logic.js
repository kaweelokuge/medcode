'use strict';
/**
 * MedCode network transactions logic
 */

/**
 * Create ProvincialAdmin
 * Performed by MedCodeAdmin
 * @param {com.medcode.network.CreateProvincialAdmin} createProvincialAdmin
 * @transaction
 */
async function createProvincialAdmin(createProvincialAdmin) {
    
    let factory = getFactory();
    let provincialAdmin = factory.newResource('com.medcode.network', 'ProvincialAdmin', createProvincialAdmin.provincialAdminId);
    provincialAdmin.firstName = createProvincialAdmin.firstName;
    provincialAdmin.lastName = createProvincialAdmin.lastName;
    
    let provincialAdminRegistry = await getParticipantRegistry('com.medcode.network.ProvincialAdmin');
    await provincialAdminRegistry.add(provincialAdmin);
}

/**
 * Create ProvincialUnit
 * Performed by MedCodeAdmin
 * @param {com.medcode.network.CreateProvincialUnit} createProvincialUnit
 * @transaction
 */
async function createProvincialUnit(createProvincialUnit) {
    
    let factory = getFactory();
    let provincialUnit = factory.newResource('com.medcode.network', 'ProvincialUnit', createProvincialUnit.provinceId);
    provincialUnit.provinceName = createProvincialUnit.provinceName;
    provincialUnit.provincialAdmin = createProvincialUnit.provincialAdmin;
    
    let provincialUnitRegistry = await getAssetRegistry('com.medcode.network.ProvincialUnit');
    await provincialUnitRegistry.add(provincialUnit);
}

/**
 * Create HospitalAdmin
 * Performed by ProvincialAdmin
 * @param {com.medcode.network.CreateHospitalAdmin} createHospitalAdmin
 * @transaction
 */
async function createHospitalAdmin(createHospitalAdmin) {
    
    let factory = getFactory();
    let hospitalAdmin = factory.newResource('com.medcode.network', 'HospitalAdmin', createHospitalAdmin.hospitalAdminId);
    hospitalAdmin.firstName = createHospitalAdmin.firstName;
    hospitalAdmin.lastName = createHospitalAdmin.lastName;
    
    let hospitalAdminRegistry = await getParticipantRegistry('com.medcode.network.HospitalAdmin');
    await hospitalAdminRegistry.add(hospitalAdmin);
}

/**
 * Create Hospital
 * Performed by HospitalAdmin
 * @param {com.medcode.network.CreateHospital} createHospital
 * @transaction
 */
async function createHospital(createHospital) {
    
    let factory = getFactory();
    let hospital = factory.newResource('com.medcode.network', 'Hospital', createHospital.hospitalId);
    hospital.hospitalName = createHospital.hospitalName;
    hospital.hospitalType = createHospital.hospitalType;
    hospital.hospitalAdmin = createHospital.hospitalAdmin;
    hospital.provincialUnit = createHospital.provincialUnit;

    let hospitalRegistry = await getAssetRegistry('com.medcode.network.Hospital');
    await hospitalRegistry.add(hospital);

}

/**
 * Create Doctor
 * Performed by HospitalAdmin
 * @param {com.medcode.network.CreateDoctor} createDoctor
 * @transaction
 */
async function createDoctor(createDoctor) {
    let factory = getFactory();
    let doctor = factory.newResource('com.medcode.network', 'Doctor', createDoctor.slmcRegNo);
    doctor.firstName = createDoctor.firstName;
    doctor.lastName = createDoctor.lastName;
    doctor.hospital = createDoctor.hospital;
    
    let doctorRegistry = await getParticipantRegistry('com.medcode.network.Doctor');
    await doctorRegistry.add(doctor);
}

/**
 * Create Patient, Create PatientRecord
 * Performed by HospitalAdmin
 * @param {com.medcode.network.CreatePatient} createPatient
 * @transaction
 */
async function createPatient(createPatient) {
    let factory = getFactory();
    let patient = factory.newResource('com.medcode.network', 'Patient', createPatient.NIC);
    patient.firstName = createPatient.firstName;
    patient.lastName = createPatient.lastName;
    patient.gender = createPatient.gender;
    patient.DOB = createPatient.DOB;
    patient.address = createPatient.address;

    let patientRegistry = await getParticipantRegistry('com.medcode.network.Patient');
    await patientRegistry.add(patient);

    let today = new Date();
    let defaultHospital = factory.newResource('com.medcode.network', 'Hospital', "H00000");
    let defaultDoctor = factory.newResource('com.medcode.network', 'Doctor', "D000000");

    let patientRecord = factory.newResource('com.medcode.network', 'PatientRecord', createPatient.NIC);
    patientRecord.recordId = createPatient.NIC;
    patientRecord.patient = patient;
    patientRecord.date = today;
    patientRecord.diagnosis = "";
    patientRecord.notes = "";
    patientRecord.treatment = "";
    patientRecord.unitType = "OPD";
    patientRecord.hospital = defaultHospital;
    patientRecord.doctor = defaultDoctor;
    patientRecord.isReferral = false;
    
    let patientRecordRegistry = await getAssetRegistry('com.medcode.network.PatientRecord');
    await patientRecordRegistry.add(patientRecord);
}

 /**
 * NIC authority can reject a patient identity
 * @param {com.medcode.network.RejectPatient} rejectPatient
 * @transaction
 */
async function rejectPatient(rejectPatient) {
    
    let patient = rejectPatient.patient;
    patient.status = "REJECTED";
    
    let patientRegistry = await getParticipantRegistry('com.medcode.network.Patient');
    await patientRegistry.update(patient);
}

 /**
 * NIC authority can accept a patient identity
 * @param {com.medcode.network.AcceptPatient} acceptPatient
 * @transaction
 */
async function acceptPatient(acceptPatient) {
    
    let patient = acceptPatient.patient;
    patient.status = "ACCEPTED";
    
    let patientRegistry = await getParticipantRegistry('com.medcode.network.Patient');
    await patientRegistry.update(patient);
}

/**
 * Update Patient Record
 * Performed by Doctor
 * @param {com.medcode.network.UpdatePatientRecord} updatePatientRecord
 * @transaction
 */
async function updatePatientRecord(updatePatientRecord) {
    
    // The current diagnosis
    const recordId = updatePatientRecord.patientRecord.recordId;
    
    const previousDate = updatePatientRecord.patientRecord.date;
    const previousDiagnosis = updatePatientRecord.patientRecord.diagnosis;
    const previousNotes = updatePatientRecord.patientRecord.notes;
    const previousTreatment = updatePatientRecord.patientRecord.treatment;
    const previousUnitType = updatePatientRecord.patientRecord.unitType;
    const previousHospital = updatePatientRecord.hospital;
    const previousDoctor = updatePatientRecord.doctor;
    const previousIsReferral = updatePatientRecord.patientRecord.isReferral;

    const previousReferredUnitType = updatePatientRecord.patientRecord.referredUnitType;
    const previousReferredHospital = updatePatientRecord.patientRecord.referredHospital;
    const previousReferredDoctor = updatePatientRecord.patientRecord.referredDoctor;
    
    // Update the asset to what the new value is. 
    updatePatientRecord.patientRecord.date = updatePatientRecord.newDate;
    updatePatientRecord.patientRecord.diagnosis = updatePatientRecord.newDiagnosis;
    updatePatientRecord.patientRecord.notes = updatePatientRecord.newNotes;
    updatePatientRecord.patientRecord.treatment = updatePatientRecord.newTreatment;
    updatePatientRecord.patientRecord.unitType = updatePatientRecord.newUnitType;
    updatePatientRecord.patientRecord.hospital = updatePatientRecord.newHospital;
    updatePatientRecord.patientRecord.doctor = updatePatientRecord.newDoctor;
    updatePatientRecord.patientRecord.isReferral = updatePatientRecord.newIsReferral;

    if (updatePatientRecord.newIsReferral) {
        updatePatientRecord.patientRecord.referredUnitType = updatePatientRecord.newReferredUnitType;
        updatePatientRecord.patientRecord.referredHospital = updatePatientRecord.newReferredHospital;
        updatePatientRecord.patientRecord.referredDoctor = updatePatientRecord.newReferredDoctor;
    }
    else{
        updatePatientRecord.patientRecord.referredUnitType = null;
        updatePatientRecord.patientRecord.referredHospital = null;
        updatePatientRecord.patientRecord.referredDoctor = null;
    }

    // Get the asset registry for the asset.
    const assetRegistry = await getAssetRegistry('com.medcode.network.PatientRecord');
    // Update the asset in the asset registry.
    await assetRegistry.update(updatePatientRecord.patientRecord);

    // Emit an event for the modified asset.
    let event = getFactory().newEvent('com.medcode.network', 'UpdatePatientRecordEvent');
    event.recordId = recordId;
    event.date = updatePatientRecord.newDate;
    event.diagnosisPrevious = previousDiagnosis;
    event.diagnosisCurrent = updatePatientRecord.newDiagnosis;
    event.isReferralCurrent = updatePatientRecord.newIsReferral;
    emit(event);
}